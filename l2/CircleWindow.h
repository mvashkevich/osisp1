#pragma once

#include "Window.h"

class CircleWindow : public BaseWindow<CircleWindow>
{
public:
	CircleWindow(HINSTANCE hInstance, int width, int height, bool scalable = false)
		: BaseWindow(), Width(width), Height(height)
	{
		SetRect(&rcClient, 0, 0, width, height);
	}
	~CircleWindow()
	{
	}
	PCWSTR ClassName() const { return L"Circle window class"; }
	LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);
private:
	const int InputBufferSize = 72;

	void HandleChar(const WPARAM& wParam);
	void HandleDestroy();
	void HandlePaint(HDC hdc);
	void HandleCreate(LPARAM lParam);
	void HandleResize();

	PTCHAR pchInputBuffer;
	RECT rcClient;
	int charPosition;
	int Width;
	int Height;
};