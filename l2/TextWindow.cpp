#include "TextWindow.h"

#include <gdiplus.h>
using namespace Gdiplus;
using namespace std;
#pragma comment (lib, "Gdiplus.lib")

//void CircleWindow::HandleChar(const WPARAM& wParam)
//{
//	switch (wParam)
//	{
//	case 0x08:
//		if (charPosition > 0)
//		{
//			charPosition--;
//			pchInputBuffer[charPosition] = 0x00;
//			InvalidateRect(hwnd, NULL, FALSE);
//		}
//		break;
//	case 0x0A:
//	case 0x1B:
//	case 0x09:
//	case 0x0D:
//		break;
//	default:
//		if (charPosition < InputBufferSize)
//		{
//			WCHAR c = WCHAR(wParam);
//			pchInputBuffer[charPosition] = c;
//			charPosition++;
//			InvalidateRect(hwnd, NULL, FALSE);
//		}
//		break;
//	}
//}

void TextWindow::HandleDestroy()
{
	RestoreDC(hBufferDC, -1);
	DeleteDC(hBufferDC);
	DeleteObject(hBufferBitmap);
	PostQuitMessage(0);
}

void TextWindow::HandleCreate(LPARAM lParam)
{
	HDC hdc = GetDC(hwnd);
	hBufferDC = CreateCompatibleDC(hdc);
	hBufferBitmap = CreateCompatibleBitmap(hdc, Width, Height);
	ReleaseDC(hwnd, hdc);
	SaveDC(hBufferDC);
	SelectObject(hBufferDC, hBufferBitmap);

	InvalidateRect(hwnd, NULL, FALSE);

	for (int i = 0; i < 10; i++)
	{
		data.push_back(vector<wstring>(10));
		for (int j = 0; j < data[i].size(); ++j)
		{
			if (i == 0)
			{
				data[i][j] = L"Caption " + to_wstring(j);
			}
			else
			{
				data[i][j] = to_wstring(i) + L"." + to_wstring(j);
			}
		}
	}
}


LRESULT TextWindow::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_PAINT:
		HDC hdc;
		PAINTSTRUCT ps;
		hdc = BeginPaint(hwnd, &ps);
		OnPaint(hdc);
		EndPaint(hwnd, &ps);
		break;
	case WM_DESTROY:
		HandleDestroy();
		break;
	case WM_GETMINMAXINFO:
	{

		PMINMAXINFO sizeInfo = (PMINMAXINFO)lParam;
		sizeInfo->ptMinTrackSize = { 300, 210 };
		break;
	}
	case WM_SIZING:
	{
		RECT clRect{};
		GetClientRect(hwnd, &clRect);
		Width = clRect.right - clRect.left;
		Height = clRect.bottom - clRect.top;

		HandleResize();
		return TRUE;
	}
	case WM_SIZE:
		Width = LOWORD(lParam);
		Height = HIWORD(lParam);
		HandleResize();
		break;
	// Explicit fall-through
	case WM_CREATE:
		HandleCreate(lParam);
	default:
		return DefWindowProc(hwnd, uMsg, wParam, lParam);
	}
	return 0;
}

void TextWindow::HandleResize()
{
	rcClient = { 0, 0, Width, Height };
	HDC hdc = GetDC(hwnd);
	RestoreDC(hBufferDC, -1);

	DeleteObject(hBufferBitmap);
	hBufferBitmap = CreateCompatibleBitmap(hdc, Width, Height);
	ReleaseDC(hwnd, hdc);
	SaveDC(hBufferDC);
	SelectObject(hBufferDC, hBufferBitmap);
	InvalidateRect(hwnd, NULL, FALSE);
}
Font* getFontSize(Graphics& graphics, const WCHAR* string, float cellWidth, float cellHeight)
{
	FontFamily   fontFamily(L"Arial");
	int fontHeight = (int)cellHeight;
	Font* font = new Font(&fontFamily, fontHeight, FontStyleBold, UnitPixel);
	RectF result{};

	graphics.MeasureString(string, -1, font, { 0,0, cellWidth, cellHeight}, StringFormat::GenericTypographic(), &result);
	while (result.Width > cellWidth || result.Height > cellHeight || result.Height == 0)
	{
		delete font;
		--fontHeight;
		font = new Font(&fontFamily, fontHeight, FontStyleBold, UnitPixel);
		graphics.MeasureString(string, -1, font, { 0,0, cellWidth, cellHeight }, StringFormat::GenericTypographic(), &result);
	}
	return font;
}

void TextWindow::OnPaint(HDC hdc)
{
	Graphics    graphics(hBufferDC);

	graphics.Clear(Color::Aqua);
	Pen pen(Color::Navy, 0);

	SolidBrush   solidBrush(Color::SlateBlue);

	int rowCount = data.size();
	int colCount = data[0].size();

	float cellWidth = Width / colCount;
	float cellHeight = Height / rowCount;
	graphics.SetTextRenderingHint(TextRenderingHintAntiAlias);
	HDC dc = graphics.GetHDC();

	bool fontFits = false;
	int fontSize = 80;


	while (!fontFits) {
		fontFits = true;
		fontSize -= 1;
		HFONT hFont = CreateFont(fontSize, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_OUTLINE_PRECIS,
			CLIP_DEFAULT_PRECIS, CLEARTYPE_QUALITY, VARIABLE_PITCH, TEXT("Jetbrains Mono"));
		SelectObject(dc, hFont);

		for (int i = 0; i < rowCount; ++i)
		{
			if (!fontFits) break;

			for (int j = 0; j < colCount; j++)
			{
				RECT rc = { cellWidth * j, cellHeight * i, cellWidth * (j + 1), cellHeight * (i + 1) };
				DrawText(dc, data[i][j].c_str(), -1, &rc, DT_WORDBREAK | DT_CALCRECT);

				if (rc.right > cellWidth* (j + 1)) {
					fontFits = fontFits && false;
					break;
				}

				if (rc.bottom > cellHeight* (i + 1)) {
					fontFits = fontFits && false;
					break;
				}


				{
					fontFits = fontFits || true;
				}
				//font = getFontSize(graphics, data[i][j].c_str(), cellWidth, cellHeight);
				//RectF rectF{ cellWidth * j, cellHeight * i, cellWidth * (j + 1), cellHeight * (i + 1) };
				//graphics.DrawRectangle(&pen, rectF);
				//graphics.DrawString(data[i][j].c_str(), -1, font, rectF, StringFormat::GenericTypographic(), &solidBrush);
				//delete font;
			}
		}

		DeleteObject(hFont);
	}

	HFONT hFont = CreateFont(fontSize, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_OUTLINE_PRECIS,
		CLIP_DEFAULT_PRECIS, CLEARTYPE_QUALITY, VARIABLE_PITCH, TEXT("Jetbrains Mono"));
	SelectObject(dc, hFont);


	for (int i = 0; i < rowCount; ++i)
	{
		for (int j = 0; j < colCount; j++)
		{
			RECT rc = { cellWidth * j, cellHeight * i, cellWidth * (j + 1), cellHeight * (i + 1) };
			DrawText(dc, data[i][j].c_str(), -1, &rc, DT_WORDBREAK);

			//font = getFontSize(graphics, data[i][j].c_str(), cellWidth, cellHeight);
			//RectF rectF{ cellWidth * j, cellHeight * i, cellWidth * (j + 1), cellHeight * (i + 1) };
			//graphics.DrawRectangle(&pen, rectF);
			//graphics.DrawString(data[i][j].c_str(), -1, font, rectF, StringFormat::GenericTypographic(), &solidBrush);
			//delete font;
		}
	}
	

	//Font* font;
	


	graphics.ReleaseHDC(dc);
	graphics.Flush();
	HDC grDC = graphics.GetHDC();
	BitBlt(hdc, 0, 0, rcClient.right - rcClient.left, rcClient.bottom - rcClient.top, grDC, 0, 0, SRCCOPY);
	graphics.ReleaseHDC(grDC);
}
