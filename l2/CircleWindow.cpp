#include "CircleWindow.h"
#include <gdiplus.h>
using namespace Gdiplus;
#pragma comment (lib, "Gdiplus.lib")

void CircleWindow::HandleChar(const WPARAM& wParam)
{
	switch (wParam)
	{
	case 0x08:
		if (charPosition > 0)
		{
			charPosition--;
			pchInputBuffer[charPosition] = 0x00;
			InvalidateRect(hwnd, NULL, FALSE);
		}
		break;
	case 0x0A:
	case 0x1B:
	case 0x09:
	case 0x0D:
		break;
	default:
		if (charPosition < InputBufferSize)
		{
			WCHAR c = WCHAR(wParam);
			pchInputBuffer[charPosition] = c;
			charPosition++;
			InvalidateRect(hwnd, NULL, FALSE);
		}
		break;
	}
}

void CircleWindow::HandleDestroy()
{
	GlobalFree((HGLOBAL)pchInputBuffer);
	RestoreDC(hBufferDC, -1);
	DeleteDC(hBufferDC);
	DeleteObject(hBufferBitmap);
	PostQuitMessage(0);
}

void CircleWindow::HandleCreate(LPARAM lParam)
{
	HDC hdc = GetDC(hwnd);
	hBufferDC = CreateCompatibleDC(hdc);
	hBufferBitmap = CreateCompatibleBitmap(hdc, Width, Height);
	ReleaseDC(hwnd, hdc);
	SaveDC(hBufferDC);
	SelectObject(hBufferDC, hBufferBitmap);

	RECT r;
	CopyRect(&r, &rcClient);
	AdjustWindowRect(&r, ((LPCREATESTRUCT)lParam)->style, FALSE);
	SetWindowPos(hwnd, 0, 0, 0, r.right - r.left, r.bottom - r.top, SWP_NOZORDER);

	pchInputBuffer = (LPTSTR)GlobalAlloc(GPTR,
		(InputBufferSize + 1) * sizeof(WCHAR));
	charPosition = 0;

	InvalidateRect(hwnd, NULL, FALSE);
}

void CircleWindow::HandleResize()
{
	rcClient = { 0, 0, Width, Height };
	HDC hdc = GetDC(hwnd);
	RestoreDC(hBufferDC, -1);

	DeleteObject(hBufferBitmap);
	hBufferBitmap = CreateCompatibleBitmap(hdc, Width, Height);
	ReleaseDC(hwnd, hdc);
	SaveDC(hBufferDC);
	SelectObject(hBufferDC, hBufferBitmap);
	InvalidateRect(hwnd, NULL, FALSE);
}


LRESULT CircleWindow::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_PAINT:
		HDC hdc;
		PAINTSTRUCT ps;
		hdc = BeginPaint(hwnd, &ps);
		HandlePaint(hdc);
		EndPaint(hwnd, &ps);
		break;
	case WM_GETMINMAXINFO:
	{

		PMINMAXINFO sizeInfo = (PMINMAXINFO)lParam;
		sizeInfo->ptMinTrackSize = { 300, 300 };
		break;
	}
	case WM_SIZING:
	{
		RECT clRect{};
		GetClientRect(hwnd, &clRect);
		Width = clRect.right - clRect.left;
		Height = clRect.bottom - clRect.top;

		HandleResize();
		return TRUE;
	}
	case WM_SIZE:
		Width = LOWORD(lParam);
		Height = HIWORD(lParam);
		HandleResize();
		break;
	case WM_CHAR:
		HandleChar(wParam);
		break;
	case WM_DESTROY:
		HandleDestroy();
		return 0;
		// Explicit fall-through
	case WM_CREATE:
		HandleCreate(lParam);
	default:
		return DefWindowProc(hwnd, uMsg, wParam, lParam);
	}
	return 0;
}

Font* getFont(Graphics& graphics, const WCHAR* string, float cellWidth)
{
	FontFamily   fontFamily(L"Arial");
	int fontHeight = (int)100;
	Font* font = new Font(&fontFamily, fontHeight, FontStyleBold, UnitPixel);
	RectF result{};
	graphics.MeasureString(string, -1, font, { 0,0 }, &result);
	while (result.Width > cellWidth)
	{
		delete font;
		--fontHeight;
		font = new Font(&fontFamily, fontHeight, FontStyleBold, UnitPixel);
		graphics.MeasureString(string, -1, font, { 0,0 }, &result);
	}
	return font;
}

void CircleWindow::HandlePaint(HDC hdc)
{
	Graphics    graphics(hBufferDC);

	graphics.Clear(Color::Wheat);
	int base = min(Width, Height);
	const float Center = base / 2;
	const int Radius = base / 4;
		const float Angle = 10.0f;
	RectF rect(0, 0, Radius * 2, Radius * 2);
	Pen pen(Color::Red, 0);
	graphics.ResetTransform();
	graphics.TranslateTransform(Center - Radius, Center - Radius);
	graphics.DrawEllipse(&pen, rect);

	FontFamily   fontFamily(L"Arial");
	Rect dot(0, 0, 20, 20);
	SolidBrush   solidBrush(Color(255, 0, 0, 255));
	pen.SetColor(Color::Magenta);
	WCHAR string[2] = L"";
	graphics.TranslateTransform(Radius, Radius);

	float charWidth = Radius * 2 * 3.14 * Angle / 360;
	Font* font = getFont(graphics, L"A", charWidth);
	float fontHeight = font->GetHeight(&graphics);
	RectF        rectF(0.0f, -Radius - fontHeight, 200.0f, 200.0f);
	for (int i = 0; i < charPosition; ++i)
	{
		if (i == 36)
		{
			rectF.Y = -Radius;
			pen.SetColor(Color::Green);
			//delete font;
			//font = new Font(&fontFamily, 10, FontStyleBold, UnitPoint);
		}
		string[0] = pchInputBuffer[i];
		graphics.RotateTransform(Angle);
		graphics.DrawString(string, -1, font, rectF, NULL, &solidBrush);
		graphics.DrawRectangle(&pen, dot);
	}
	graphics.ResetTransform();
	graphics.Flush();
	HDC grDC = graphics.GetHDC();
	BitBlt(hdc, 0, 0, rcClient.right - rcClient.left, rcClient.bottom - rcClient.top, grDC, 0, 0, SRCCOPY);
	graphics.ReleaseHDC(grDC);
	delete font;
}
