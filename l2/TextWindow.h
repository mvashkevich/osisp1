#pragma once

#include "Window.h"
#include <vector>
#include <string>

class TextWindow : public BaseWindow<TextWindow>
{
public:
	TextWindow(HINSTANCE hInstance)
		: BaseWindow()
	{
	}
	~TextWindow()
	{
	}
	PCWSTR ClassName() const { return L"Text window class"; }
	LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);
	void HandleResize();
private:
	//void HandleChar(const WPARAM& wParam);
	void HandleDestroy();
	void OnPaint(HDC hdc);
	void HandleCreate(LPARAM lParam);

	std::vector<std::vector<std::wstring>> data;
	RECT rcClient;
	int Width;
	int Height;
};