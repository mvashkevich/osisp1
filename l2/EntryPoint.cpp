#include <Windows.h>
#include <gdiplus.h>
#include "Window.h"
#include <shellscalingapi.h>
#include "CircleWindow.h"
#include "TextWindow.h"
using namespace Gdiplus;
#pragma comment (lib, "Gdiplus.lib")
#pragma comment (lib, "Shcore.lib")

int WINAPI wWinMain(
	HINSTANCE hInstance, // handle to instance (module) - for identifying the exe in memory
	HINSTANCE hPrevInstance, // for 16-bit windows
	PWSTR pCmdLine,
	int nCmdShow) // minimized/maximized/normal window
{
	SetProcessDpiAwareness(PROCESS_PER_MONITOR_DPI_AWARE);
	GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR gdiplusToken;

	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
	const int CircleWindowSize = 600;
	CircleWindow circleWindow(hInstance, CircleWindowSize, CircleWindowSize);
	TextWindow textWindow(hInstance);

	BOOL textCreated = textWindow.Create(hInstance, L"Lab 2: Text", WS_OVERLAPPEDWINDOW);
	BOOL circleCreated = circleWindow.Create(hInstance, L"Lab 2: Circle", WS_OVERLAPPEDWINDOW);
	if (!textCreated && !circleCreated)
	{
		return 0;
	}

	ShowWindow(circleWindow.GetHandle(), nCmdShow);
	ShowWindow(textWindow.GetHandle(), SW_MAXIMIZE);

	MSG msg = { };
	while (GetMessage(&msg, NULL, 0, 0)) // hWnd, fin
	{
		TranslateMessage(&msg); // translates virtual-key messages into character messages
		DispatchMessage(&msg);
	}
	GdiplusShutdown(gdiplusToken);

	return 0;
}

