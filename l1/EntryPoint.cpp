#include <Windows.h>
#include "resource.h"

template <class DerivedType>
class BaseWindow
{
public:
	static LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		DerivedType* pThis = NULL;

		if (uMsg == WM_NCCREATE)
		{
			CREATESTRUCT* pCreate = (CREATESTRUCT*)lParam;
			pThis = (DerivedType*)pCreate->lpCreateParams;
			SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR)pThis);

			pThis->hwnd = hWnd;
		}
		else
		{
			pThis = (DerivedType*)GetWindowLong(hWnd, GWLP_USERDATA);
		}
		if (pThis)
		{
			return pThis->HandleMessage(uMsg, wParam, lParam);
		}
		else
		{
			return DefWindowProc(hWnd, uMsg, wParam, lParam);
		}
	}

	BaseWindow() : hwnd(NULL) {}

	BOOL Create(HINSTANCE hInstance, PCWSTR lpWindowName,
		DWORD dwStyle,
		DWORD dwExStyle = 0,
		int x = CW_USEDEFAULT,
		int y = CW_USEDEFAULT,
		int nWidth = CW_USEDEFAULT,
		int nHeight = CW_USEDEFAULT,
		HWND hWndParent = 0,
		HMENU hMenu = 0)
	{
		WNDCLASS wc = {};
		wc.lpfnWndProc = DerivedType::WindowProc;
		wc.hInstance = hInstance;
		wc.lpszClassName = ClassName();

		RegisterClass(&wc);

		hwnd = CreateWindowEx(dwExStyle, ClassName(), lpWindowName, dwStyle, x, y, nWidth, nHeight, hWndParent, hMenu, hInstance, this);

		return hwnd ? TRUE : FALSE;
	}

	HWND GetHandle() const { return hwnd; }
protected:
	virtual PCWSTR ClassName() const = 0;
	virtual LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam) = 0;

	HWND hwnd;
};

class MainWindow : public BaseWindow<MainWindow>
{
public:
	MainWindow(HINSTANCE hInstance) : BaseWindow(), hAmoebaDC(NULL), hBufferBitmap(NULL),
		hBufferDC(NULL), x(0), y(0), canMove(true), scaling(100), bounceForce(MinBounceForce)
	{
		hBitmap = LoadBitmap(hInstance, MAKEINTRESOURCE(IDB_BITMAP1));
		BITMAP bitmap;
		GetObject(hBitmap, sizeof(bitmap), &bitmap);
		amoebaHeight = bitmap.bmHeight;
		amoebaWidth = bitmap.bmWidth;
		hBackgroundBrush = CreateSolidBrush(0x00FFFFFF);
		SetRect(&rcClient, 0, 0, Width, Height);
	}
	~MainWindow()
	{
		DeleteObject(hBitmap);
		DeleteObject(hBackgroundBrush);
	}
	PCWSTR ClassName() const { return L"Window class"; }
	LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);
private:
	static const int Step = 5;
	static const int Width = 1280;
	static const int Height = 720;
	static const int ScalingStep = 5;
	static const int MinScaling = 50;
	static const int MaxScaling = 150;
	static const int RedrawTimerDuration = 10;
	static const int BouncingDuration = 1000;
	static const int RedrawTimerID = 0;
	static const int BouncingTimerID = 1;
	static const int InitialBounceAcceleration = 1000;
	static const int MinBounceForce = 1;
	static const int MaxBounceForce = 5;
	enum class Direction {
		Down, Left, Up, Right
	};
	HBITMAP hBitmap;
	HDC hAmoebaDC;
	HDC hBufferDC;
	HBITMAP hBufferBitmap;
	HBRUSH hBackgroundBrush;
	int x;
	int y;
	int amoebaWidth;
	int amoebaHeight;
	int scaling;
	RECT rcClient;
	bool canMove;
	int bounceAcceleration;
	int bounceX;
	int bounceY;
	int bounceForce;

	void HandleCreate(LPARAM lParam);
	void HandleMove(WPARAM wParam);
	void HandleWheel(WPARAM wParam);
	void HandleDestroy();
	void HandlePaint();
	void HandleBouncing(Direction direction);
	void HandleTimerAnimation(WPARAM wParam);
	int GetScaledWidth() const {
		return amoebaWidth * scaling / 100;
	}
	int GetScaledHeight() const {
		return amoebaHeight * scaling / 100;
	}
};

int WINAPI wWinMain(
	HINSTANCE hInstance, // handle to instance (module) - for identifying the exe in memory
	HINSTANCE hPrevInstance, // for 16-bit windows
	PWSTR pCmdLine,
	int nCmdShow) // minimized/maximized/normal window
{
	MainWindow window(hInstance);

	if (!window.Create(hInstance, L"Lab 1", WS_OVERLAPPEDWINDOW & ~(WS_SIZEBOX | WS_MAXIMIZEBOX)))
	{
		return 0;
	}

	ShowWindow(window.GetHandle(), nCmdShow);

	MSG msg = { };
	while (GetMessage(&msg, NULL, 0, 0)) // hWnd, fin
	{
		TranslateMessage(&msg); // translates virtual-key messages into character messages
		DispatchMessage(&msg);
	}

	return 0;
}

LRESULT MainWindow::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if (uMsg == WM_CREATE)
	{
		HandleCreate(lParam);
		return DefWindowProc(hwnd, uMsg, wParam, lParam);
	}

	switch (uMsg)
	{
	case WM_MOUSEWHEEL:
		HandleWheel(wParam);
		break;
	case WM_KEYDOWN:
		HandleMove(wParam);
		break;
	case WM_PAINT:
		HandlePaint();
		break;
	case WM_DESTROY:
		HandleDestroy();
		break;
	case WM_TIMER:
		HandleTimerAnimation(wParam);
		break;
	default:
		return DefWindowProc(hwnd, uMsg, wParam, lParam);
	}
	return 0;
}

void MainWindow::HandleCreate(LPARAM lParam)
{
	HDC hdc = GetDC(hwnd);
	hAmoebaDC = CreateCompatibleDC(hdc);
	hBufferDC = CreateCompatibleDC(hdc);
	hBufferBitmap = CreateCompatibleBitmap(hdc, Width, Height);
	ReleaseDC(hwnd, hdc);
	SaveDC(hAmoebaDC);
	SelectObject(hAmoebaDC, hBitmap);
	SaveDC(hBufferDC);
	SelectObject(hBufferDC, hBufferBitmap);

	RECT r;
	CopyRect(&r, &rcClient);
	AdjustWindowRect(&r, ((LPCREATESTRUCT)lParam)->style, FALSE);
	SetWindowPos(hwnd, 0, 0, 0, r.right - r.left, r.bottom - r.top, SWP_NOZORDER);

	InvalidateRect(hwnd, NULL, TRUE);
}

void MainWindow::HandleWheel(WPARAM wParam)
{
	auto keyStates = GET_KEYSTATE_WPARAM(wParam);
	auto delta = GET_WHEEL_DELTA_WPARAM(wParam);
	if ((~keyStates & MK_SHIFT) && canMove)
	{
		if (delta > 0 && scaling + ScalingStep <= MaxScaling)
		{
			scaling += ScalingStep;
			InvalidateRect(hwnd, NULL, TRUE);
		}
		else if (delta < 0 && scaling - ScalingStep >= MinScaling)
		{
			scaling -= ScalingStep;
			InvalidateRect(hwnd, NULL, TRUE);
		}
	}
	else if (keyStates & MK_SHIFT)
	{
		if (delta < 0 && bounceForce < MaxBounceForce)
		{
			++bounceForce;
		}
		else if (delta > 0 && bounceForce > MinBounceForce)
		{
			--bounceForce;
		}
	}
	
}

void MainWindow::HandleDestroy()
{
	RestoreDC(hAmoebaDC, -1);
	RestoreDC(hBufferDC, -1);
	DeleteDC(hAmoebaDC);
	DeleteDC(hBufferDC);
	DeleteObject(hBufferBitmap);

	PostQuitMessage(0); // put WM_QUIT to message queue -> return 0 from GetMessage
}

void MainWindow::HandlePaint()
{
	HDC hdc;
	PAINTSTRUCT ps;
	hdc = BeginPaint(hwnd, &ps);
	FillRect(hBufferDC, &rcClient, hBackgroundBrush);
	StretchBlt(hBufferDC, x, y, GetScaledWidth(), GetScaledHeight(), hAmoebaDC, 0, 0, amoebaWidth, amoebaHeight, SRCCOPY);
	BitBlt(hdc, 0, 0, rcClient.right - rcClient.left, rcClient.bottom - rcClient.top, hBufferDC, 0, 0, SRCCOPY);
	EndPaint(hwnd, &ps);
}

void MainWindow::HandleBouncing(Direction direction)
{
	canMove = false;
	SetTimer(GetHandle(), RedrawTimerID, RedrawTimerDuration, NULL);
	SetTimer(GetHandle(), BouncingTimerID, BouncingDuration / bounceForce, NULL);
	bounceX = 0;
	bounceY = 0;
	bounceAcceleration = InitialBounceAcceleration;
	switch (direction)
	{
	case Direction::Down:
		y = 0;
		bounceY = 1;
		break;
	case Direction::Left:
		x = Width - GetScaledWidth();
		bounceX = -1;
		break;
	case Direction::Up:
		y = Height - GetScaledHeight();
		bounceY = -1;
		break;
	case Direction::Right:
		x = 0;
		bounceX = 1;
		break;
	}
}

void MainWindow::HandleTimerAnimation(WPARAM wParam)
{
	switch (wParam)
	{
	case BouncingTimerID:
		KillTimer(GetHandle(), RedrawTimerID);
		KillTimer(GetHandle(), BouncingTimerID);
		canMove = true;
		break;
	case RedrawTimerID:
		x = x + Step * bounceX * bounceAcceleration / InitialBounceAcceleration / bounceForce;
		y = y + Step * bounceY * bounceAcceleration / InitialBounceAcceleration / bounceForce;
		bounceAcceleration -= RedrawTimerDuration;
		InvalidateRect(GetHandle(), NULL, TRUE);
	}
}

void MainWindow::HandleMove(WPARAM wParam)
{
	if (canMove)
	{
		switch (wParam)
		{
		case VK_LEFT:
			if (x - Step >= 0)
			{
				x -= Step;
			}
			else
			{
				HandleBouncing(Direction::Right);
			}
			break;
		case VK_RIGHT:
			if (x + Step + GetScaledWidth() <= Width)
			{
				x += Step;
			}
			else
			{
				HandleBouncing(Direction::Left);
			}
			break;
		case VK_UP:
			if (y - Step >= 0)
			{
				y -= Step;
			}
			else
			{
				HandleBouncing(Direction::Down);
			}
			break;
		case VK_DOWN:
			if (y + Step + GetScaledHeight() <= Height)
			{
				y += Step;
			}
			else
			{
				HandleBouncing(Direction::Up);
			}
			break;
		}
	}
	InvalidateRect(hwnd, NULL, FALSE);
}
